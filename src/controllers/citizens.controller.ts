import { Request, Response } from "express";
import { Citizen } from "../models/Citizen";
import { MongoClient } from "mongodb";
import dotenv from 'dotenv';

//Inicializar modulo de variables de entorno.
dotenv.config();

//Inicializar conexión a base de datos.
const uri = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/`;
const client = new MongoClient(uri);
client.connect().then(res => {
    console.log(`Conectado a la Base de datos en ${uri}`);
}).catch(e => {
    console.error("Error en conectado a la Base de datos");
    console.error(e);
})
const database = client.db('springfield-citizens-db');
database.collection('citizens').createIndex({ identification: 1 }, { unique: true })
    .then(res => {
        console.log("Indice creado");
        console.log(res);
    }).catch(e => {
        console.log("Error al crear indice creado");
        console.log(e);
    })


async function runGet(filters?: any) {
    let citizens: Array<any> = [];
    try {
        //Verificar si el get es con filtros
        if (Object.keys(filters).length === 0) {

            //Retornar todos los datos.
            citizens = await database.collection('citizens').find().toArray();
        } else {

            let query: any = {};

            //Adjustar filtros

            let minAge: number = 0;
            let maxAge: number = 0;
            let today: Date = new Date();
            let dateMinResult: Date = today;
            let dateMaxResult: Date = today;

            Object.entries(filters).forEach(([key, value]) => {


                if (typeof value == 'string') {
                    //Convertir todo a minusculas en caso de texto. Ej: /citizens?gender=M sera igual a /citizens?gender=m
                    query[key] = value.toLowerCase().trim();

                    /* Filtrar por: Está vivo. */
                    //Igual a true o vacio retorna todos los vivos, false o cualquier otro valor retornara todos los que no estan vivos.
                    if (key == 'isAlive') {
                        query[key] = value.toLowerCase().trim() == 'true' || value.toLowerCase().trim() == '';
                    }

                    /* Filtrar por: Rango de edad .*/
                    //Calcular rango de fechas desde el rango de edad. solo tomara efecto el un filtro entre "minAge a maxAge" o "isAdult" ya que ambos hacen uso del campo birthday.
                    if (key == 'minAge' || key == 'maxAge') {
                        if (key == 'minAge') {
                            minAge = +value;
                            dateMinResult = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());

                            //Eliminar minAge del objeto query
                            delete query[key];
                        }

                        if (key == 'maxAge') {
                            maxAge = +value;
                            dateMaxResult = new Date(today.getFullYear() - (maxAge + 1), today.getMonth(), today.getDate());

                            //Eliminar maxAge del objeto query
                            delete query[key];
                        }

                        //Agregar a la query el rango de fechas de nacimientos.
                        if (dateMinResult == today) {
                            query.birthdate = { $gt: dateMaxResult }
                        } else if (dateMaxResult == today) {
                            query.birthdate = { $lt: dateMinResult }
                        } else {
                            query.birthdate = { $gt: dateMaxResult, $lt: dateMinResult }
                        }
                    } else {
                        //Calcular rango de fechas desde el rango de edad.
                        /* Filtrar por: Es mayor de edad .*/
                        //Igual a true o vacio retorna todos los adultos mayores de 18 años, false o cualquier otro valor retornara todos los menores de 18 años.
                        if (key == 'isAdult') {
                            let today: Date = new Date();
                            let dateMinResult: Date = today;
                            let dateMaxResult: Date = today;
                            if (value.toLowerCase().trim() == 'true' || value.toLowerCase().trim() == '') {
                                let minAge: number = 18;
                                dateMinResult = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
                                query.birthdate = { $lt: dateMinResult }
                            } else {
                                let maxAge: number = 18;
                                dateMaxResult = new Date(today.getFullYear() - maxAge, today.getMonth(), today.getDate());
                                query.birthdate = { $gt: dateMaxResult }
                            }
                            delete query[key];
                        }
                    }


                }
            });

            //Retornar datos encontrados
            citizens = await database.collection('citizens').find(query).toArray();
        }
    } finally {
        return citizens;
    }
}

const getCitizens = async (req: Request, res: Response) => {
    let response: any = await runGet(req.query).catch();
    if (response.length > 0) {
        res.statusCode = 200;
        res.send(
            response
        );
    } else {
        res.statusCode = 204;
        res.send()
    }
};

const addCitizen = async (req: Request, res: Response) => {
    const body: any = req.body;
    try {
        const citizen = new Citizen(
            {
                address: body.address.toLowerCase(),
                birthdate: new Date(body.birthdate),
                gender: body.gender.toLowerCase(),
                identification: body.identification,
                job: body.job.toLowerCase(),
                lastname: body.lastname.toLowerCase(),
                name: body.name.toLowerCase(),
                isAlive: body.isAlive
            }
        );
        database.collection('citizens').insertOne(citizen).then(doc => {
            res.send(
                doc
            );
        }).catch(e => {
            console.error("e", e);
            res.statusCode = 503;
            res.statusMessage = e;
            res.send();
        });
    } catch (e: any) {
        res.statusCode = 503;
        res.statusMessage = e.toString();
        res.send();
    }
}

const editCitizen = async (req: Request, res: Response) => {
    const identification = req.params.identification;
    const citizen = new Citizen(req.body);
    try {
        let query = {
            identification: identification
        };
        let values = {
            $set: {
                address: citizen.address.toLowerCase(),
                birthdate: new Date(citizen.birthdate),
                gender: citizen.gender.toLowerCase(),
                job: citizen.job.toLowerCase(),
                lastname: citizen.lastname.toLowerCase(),
                name: citizen.name.toLowerCase(),
                isAlive: citizen.isAlive
            }
        };

        database.collection('citizens').updateOne(query, values).then(doc => {

            //Verificar que el ciudadano se encontro y modifico correctamente, de lo contrario arrojar 404 si no fue encontrado en la BD.
            if (doc.matchedCount > 0) {
                res.send(
                    doc
                );
            } else {
                res.statusCode = 404;
                res.statusMessage = "Document not found in DB."
                res.send();
            }

        }).catch(e => {
            console.error("e", e);
            res.statusCode = 503;
            res.statusMessage = e;
            res.send();
        });

    } catch (e: any) {
        res.statusCode = 503;
        res.statusMessage = e.toString();
        res.send();
    }
}

export default {
    getCitizens,
    addCitizen,
    editCitizen
}
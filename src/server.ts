import app from './app';

//Asignar puerto para al App.
const port: number = process.env.APP_PORT;

//Iniciar servidor.
app.listen(port, () => {
  console.log(`Servidor corriendo en http://localhost:${port}`);
});
import { Router } from "express";
import CitizensController from '../controllers/citizens.controller';

//Inicializar modulo Router.
const router = Router();

//Obtener los ciudadanos sin y con filtros. 
/* 
    //Obtener todos
    -GET http://{{HOST}}:3030/springfield/citizens

    //Filtro por campo.
    -GET http://{{HOST}}:3030/springfield/citizens?gender=m&name=homero

    //Filtro por "Esta vivo".
    -GET http://{{HOST}}:3030/springfield/citizens?isAlive
    -GET http://{{HOST}}:3030/springfield/citizens?isAlive=false

    //Filtro por "Rango de edad".
    -GET http://{{HOST}}:3030/springfield/citizens?minAge=20&maxAge=40

    //Filtro por "Es mayor de edad".
    -GET http://{{HOST}}:3030/springfield/citizens?isAdult
    -GET http://{{HOST}}:3030/springfield/citizens?isAdult=false
*/
router.get('/', CitizensController.getCitizens);

//Guardar en BD el ciudadado.
/* 
    POST http://{{HOST}}:3030/springfield/citizens 
    Body: 
    {
        "address": "infantil 32B",
        "birthdate": 1950-05-12T00:00:00.000+00:00,
        "gender": "m",
        "identification": "9999999-1",
        "job": "supervisor de planta nuclear",
        "lastname": "simpson",
        "name": "homero",
        "isAlive": true
    }
*/
router.post('/', CitizensController.addCitizen);

//Editar en BD el ciudadado.
/* 
    PUT http://{{HOST}}:3030/springfield/citizens/9999999-1
    Body: 
    {
        "address": "infantil 32B",
        "birthdate": {},
        "gender": "m",
        "job": "supervisor de planta nuclear",
        "lastname": "simpson",
        "name": "homero",
        "isAlive": true
    }
*/
router.put('/:identification', CitizensController.editCitizen);

export { router };
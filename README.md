# Springfields Citizens API

## Descripción
API de Mantenedor de ciudadanos para el registro civil de la maravillosa ciudad de Springfield :)

## Prerrequisitos
Considerar tener instalado:

- Docker y Docker Compose.
- NodeJS v16.x.
- MongoDB v6.x y MongoDb Compass(opcional).

## Instalación y ejecución

- Construir, montar la imagen y correrlo dentro de Docker.
```bash
docker-compose up
```

- Correrlo local.

```bash
npm start
```

## Configuración
La Base de datos debe encontraré construida con MongoDB bajo el nombre springfield-citizens-db.

Para configurar la conexión a la base de datos se debe modificar las variables de entornos DB_HOST y DB_PORT que se encuentran en el archivo .env (para el caso de correrlo en local) o .env.docker(en caso de correrlo en un contenedor docker) en la raíz del proyecto.

```bash
#Archivo .env o .env.docker
# Conexión a Base de datos
DB_HOST = "mongodb"
DB_PORT = 27017
```

Nota: En caso de ejecutarlo en un contenedor docker asegurarse que en el archivo docker-compose.yml en el apartado de mongo>ports este tambien el mismo puerto a exponer.

```bash
#Archivo docker-compose.yml
  mongodb:
    container_name: mongodb
    image: mongo:6.0
    volumes:
      - ~/mongo:/data/db
    ports:
      - "27017:27017"
```

## Usos

La API cuenta con los métodos GET, POST, PUT, para la obtención de datos de los ciudadanos, agregarlos a la base de datos y editarlos.

```bash
#Obtener todos
-GET http://{{HOST}}:3030/springfield/citizens

#Filtro por campo.
-GET http://{{HOST}}:3030/springfield/citizens?gender=m&name=homero

#Filtro por "Esta vivo".
-GET http://{{HOST}}:3030/springfield/citizens?isAlive
-GET http://{{HOST}}:3030/springfield/citizens?isAlive=false

#Filtro por "Rango de edad".
-GET http://{{HOST}}:3030/springfield/citizens?minAge=20&maxAge=40

#Filtro por "Es mayor de edad".
-GET http://{{HOST}}:3030/springfield/citizens?isAdult
-GET http://{{HOST}}:3030/springfield/citizens?isAdult=false

#Response
[
    {
        "_id": "63f377a0d0e33567de97c8a7",
        "address": "infantil 32b",
        "birthdate": "1991-02-12T00:00:00.000Z",
        "gender": "f",
        "identification": "8888888-1",
        "job": "ama de casa",
        "lastname": "simpson",
        "name": "marge",
        "isAlive": true
    }
    .
    .
    .
]
```

```bash
#Guardar en BD el ciudadado.
POST http://{{HOST}}:3030/springfield/citizens 
Body: 
{
    "address": "infantil 32B",
    "birthdate": 1950-05-12T00:00:00.000+00:00,
    "gender": "m",
    "identification": "9999999-1",
    "job": "supervisor de planta nuclear",
    "lastname": "simpson",
    "name": "homero",
    "isAlive": true
}

Response: 
{
    "acknowledged": true,
    "insertedId": "63f68f5a7e7767d004d00caa"
}    

```

```bash
#Editar en BD el ciudadado.
PUT http://{{HOST}}:3030/springfield/citizens/9999999-1
Body: 
{
    "address": "infantil 32B",
    "birthdate": {},
    "gender": "m",
    "identification": "9999999-1",
    "job": "supervisor de planta nuclear",
    "lastname": "simpson",
    "name": "homero",
    "isAlive": true
}

Response:
{
    "acknowledged": true,
    "modifiedCount": 1,
    "upsertedId": null,
    "upsertedCount": 0,
    "matchedCount": 1
}
```

NOTA: Se deja una colección de Postman en la raíz del proyecto llamado Springfield-Citizens-API.postman_collection.json.

## Pruebas unitarias

Para correr los tests unitarios ejecutar el siguiente comando:

```bash
npm test
```
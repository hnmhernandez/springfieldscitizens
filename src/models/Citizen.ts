export class Citizen {
    address: string;
    birthdate: Date;
    gender: 'f' | 'm';
    identification: string;
    job: string;
    lastname: string;
    name: string;
    isAlive: boolean;

    public constructor(obj: any) {
        this.address = obj.address;
        this.birthdate = obj.birthdate;
        this.gender = obj.gender;
        this.identification = obj.identification;
        this.job = obj.job;
        this.lastname = obj.lastname;
        this.name = obj.name;
        this.isAlive = obj.isAlive;
    }


}
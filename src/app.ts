import express, { Application, Request, Response, NextFunction } from "express";
import { router as citizensRoutes } from "./routes/citizens.routes";

//Inicializar app.
const app: Application = express();
app.use(express.urlencoded({ extended: true }))
app.use(express.json());

//Rutas para obtener, editar y agregar datos de los ciudadanos.
app.use("/springfield/citizens", citizensRoutes);

//Raiz de la API
app.use("/", (req: Request, res: Response, next: NextFunction): void => {
  res.json({ message: "Bienvenido a la API del registro civil de la maravillosa de Ciudad de Springfield :)!" });
});

export default app;
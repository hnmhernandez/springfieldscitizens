import request from "supertest";
import app from "../src/app";
import { Citizen } from "./models/Citizen";
import { MongoClient } from "mongodb";

import dotenv from 'dotenv';

//Inicializar modulo de variables de entorno.
dotenv.config();

//Inicializar conexión a base de datos.
const uri = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/`;

const client = new MongoClient(uri);
beforeAll(done => {
  client.connect();
  done();
})

afterAll(done => {
  client.close();
  done();
})


describe("Probando server.js", () => {
  test("Inicio de servidor", async () => {
    expect(2 + 2).toEqual(4);
  });
});

describe("Probando app.js", () => {
  test("Inicio de la App", async () => {
    const res = await request(app).get("/");
    expect(res.body).toEqual({ message: "Bienvenido a la API del registro civil de la maravillosa de Ciudad de Springfield :)!" });
  });
});

describe("Probando endpoints", () => {
  test("Obtenener todos los ciudadanos", async () => {
    const res = await request(app).get("/springfield/citizens");
    if (res.status == 204) {
      expect(res.status).toEqual(204);
    } else if (res.status == 200) {
      expect(res.status).toEqual(200);
    }
  });

  test("Obtenener ciudadanos por campo", async () => {
    const res = await request(app).get("/springfield/citizens?lastname=simpson");
    if (res.status == 204) {
      expect(res.status).toEqual(204);
    } else if (res.status == 200) {
      expect(res.status).toEqual(200);
    }
  });

  test("Obtenener ciudadanos por rango de edad", async () => {
    const res = await request(app).get("/springfield/citizens?minAge=13&maxAge=35");
    if (res.status == 204) {
      expect(res.status).toEqual(204);
    } else if (res.status == 200) {
      expect(res.status).toEqual(200);
    }
  });

  test("Obtenener ciudadanos por filtro 'Esta vivo'", async () => {
    const res = await request(app).get("/springfield/citizens?isAlive");
    if (res.status == 204) {
      expect(res.status).toEqual(204);
    } else if (res.status == 200) {
      expect(res.status).toEqual(200);
    }
  });

  test("Obtenener ciudadanos por filtro 'Es mayor de edad'", async () => {
    const res = await request(app).get("/springfield/citizens?isAdult");
    if (res.status == 204) {
      expect(res.status).toEqual(204);
    } else if (res.status == 200) {
      expect(res.status).toEqual(200);
    }
  });

  const citizen = new Citizen(
    {
      "address": "los alerces 353",
      "birthdate": "1950-05-12T00:00:00.000+00:00",
      "gender": "m",
      "identification": "123123-1",
      "job": "cajero",
      "lastname": "nahasapeemapetilon",
      "name": "apu",
      "isAlive": true
    }
  )
  test("Agregar ciudadano", async () => {
    const res = await request(app).post("/springfield/").send(citizen);
    expect(res.status).toEqual(200);
  });

  test("Editar ciudadano", async () => {
    const res = await request(app).put("/springfield/123123-1").send(citizen);
    expect(res.status).toEqual(200);
  });


});